use anyhow::{anyhow, Result};
use lazy_static::lazy_static;
use regex::{Captures, Regex};

#[derive(Debug)]
pub struct TimeConverter {
    time: String,
}

impl TimeConverter {
    pub fn new(time: String) -> Self {
        Self { time }
    }

    pub fn convert(&self) -> Result<String> {
        lazy_static! {
            static ref REGEX_MS: Regex = Regex::new(r"^\d+$").unwrap();
            static ref REGEX_TIME: Regex =
                Regex::new(r"^(?P<h>\d+:)??(?P<m>\d{1,2}:)?(?P<s>\d{1,2})(?P<ms>\.\d{1,3})?$")
                    .unwrap();
        }
        let time = self.time.as_str();
        if REGEX_MS.is_match(time) {
            self.convert_ms_to_time(time)
        } else if let Some(captures) = REGEX_TIME.captures(time) {
            self.convert_time_to_ms(captures)
        } else {
            Err(anyhow!("Time not valid: {}", self.time))
        }
    }

    fn convert_ms_to_time(&self, ms: &str) -> Result<String> {
        let ms = ms.parse::<u64>()?;
        let h = ms / (60 * 60 * 1000);
        let m = (ms / (60 * 1000)) % 60;
        let s = (ms / 1000) % 60;
        let ms = ms % 1000;

        Ok(if h == 0 {
            format!("{:02}:{:02}.{:03}", m, s, ms)
        } else {
            format!("{}:{:02}:{:02}.{:03}", h, m, s, ms)
        })
    }

    fn convert_time_to_ms(&self, captures: Captures) -> Result<String> {
        let h = captures.name("h").map_or(Ok(0u64), |h| {
            h.as_str().trim_end_matches(':').parse::<u64>()
        });
        let m = captures.name("m").map_or(Ok(0u64), |m| {
            m.as_str().trim_end_matches(':').parse::<u64>()
        });
        let s = captures
            .name("s")
            .map_or(Ok(0u64), |s| s.as_str().parse::<u64>());
        let ms = captures.name("ms").map_or(Ok(0u64), |ms| {
            format!("{:0<3}", ms.as_str().trim_start_matches('.')).parse::<u64>()
        });

        match (h, m, s, ms) {
            (Ok(h), Ok(m), Ok(s), Ok(ms)) if m < 60 && s < 60 => Ok(format!(
                "{}",
                h * 60 * 60 * 1000 + m * 60 * 1000 + s * 1000 + ms
            )),
            _ => Err(anyhow!("Time not valid: {}", self.time)),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;

    #[test]
    fn time_converter_should_store_ms_as_passed() {
        let tc = TimeConverter::new("1000".to_string());
        assert_eq!("1000", tc.time);
    }

    #[test]
    fn convert_should_return_correct_value() {
        let test_cases = HashMap::from([
            ("387", "00:00.387".to_string()),
            ("1000", "00:01.000".to_string()),
            ("300042", "05:00.042".to_string()),
            ("900123", "15:00.123".to_string()),
            ("10069813", "2:47:49.813".to_string()),
            ("00:00.387", "387".to_string()),
            ("00:01.000", "1000".to_string()),
            ("00:1.000", "1000".to_string()),
            ("05:00.042", "300042".to_string()),
            ("15:00.123", "900123".to_string()),
            ("2:47:49.813", "10069813".to_string()),
            ("1.6", "1600".to_string()),
            ("1.06", "1060".to_string()),
            ("1.006", "1006".to_string()),
        ]);

        for (key, value) in test_cases {
            let tc = TimeConverter::new(key.to_string());
            match tc.convert() {
                Ok(result) => assert_eq!(value, result),
                Err(e) => panic!("Error: {} for input {}", e, key),
            }
        }
    }

    #[test]
    fn convert_should_return_error_when_invalid_time() {
        let test_cases = vec![
            "apple",
            "00:00:00:00",
            "1:000:00.123",
            "1:000",
            "1:000.123",
            "123.",
            "123.1234",
            "60:00",
            "1:60",
            "0:99",
            "1.",
        ];

        for value in test_cases {
            let tc = TimeConverter::new(value.to_string());
            match tc.convert() {
                Ok(result) => panic!("Expected error, got {} for input {}", result, value),
                Err(e) => assert_eq!(format!("Time not valid: {}", value), e.to_string()),
            }
        }
    }
}
