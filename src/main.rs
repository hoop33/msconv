use clap::Parser;

use msconv::TimeConverter;

/// Convert milliseconds to formatted time or formatted time to milliseconds.
#[derive(Parser, Debug)]
#[clap(version, about, long_about = None)]
struct Args {
    #[clap(value_parser, help = "time (HH:MM:SS.mmm or milliseconds)")]
    time: String,
}

fn main() {
    let time = Args::parse().time;
    match TimeConverter::new(time).convert() {
        Ok(time) => println!("{}", time),
        Err(e) => eprintln!("Error: {}", e),
    }
}
