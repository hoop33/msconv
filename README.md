# msconv

> Converts milliseconds to formatted time and formatted time to milliseconds

![msconv logo](msconv.svg)

## Usage

```console
$ msconv --version
msconv 0.2.3

$ msconv --help
Convert milliseconds to formatted time or formatted time to milliseconds

Usage: msconv <TIME>

Arguments:
  <TIME>  time (HH:MM:SS.mmm or milliseconds)

Options:
  -h, --help     Print help
  -V, --version  Print version

$ msconv 3000000
50:00.000

$ msconv 10069813
2:47:49.813

$ msconv 3:45
225000

$ msconv 1:23:45.678
5025678

$ msconv 5025678
1:23:45.678
```

## License

Copyright &copy; 2023 Rob Warner

Licensed under the [MIT License](https://hoop33.mit-license.org/)
